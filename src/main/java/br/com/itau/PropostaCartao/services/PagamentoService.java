package br.com.itau.PropostaCartao.services;

import br.com.itau.PropostaCartao.DTOs.EntradaPagamentoDTO;
import br.com.itau.PropostaCartao.DTOs.SaidaPagamentoDTO;
import br.com.itau.PropostaCartao.models.Cartao;
import br.com.itau.PropostaCartao.models.Pagamento;
import br.com.itau.PropostaCartao.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoService cartaoService;


    public SaidaPagamentoDTO incluirPagamento(EntradaPagamentoDTO entradaPagamentoDTO){

        Cartao objCartao = cartaoService.consultarCartaoPorId(entradaPagamentoDTO.getCartao_id());

        if (objCartao != null){

            Pagamento objPagto = new Pagamento();
            objPagto.setCartao(objCartao);
            objPagto.setDescrição(entradaPagamentoDTO.getDescricao());
            objPagto.setValor(entradaPagamentoDTO.getValor());
            pagamentoRepository.save(objPagto);

            SaidaPagamentoDTO saidaPagamentoDTO = new SaidaPagamentoDTO();
            saidaPagamentoDTO.setId(objPagto.getId());
            saidaPagamentoDTO.setCartao_id(objCartao.getId());
            saidaPagamentoDTO.setDescricao(entradaPagamentoDTO.getDescricao());
            saidaPagamentoDTO.setValor(entradaPagamentoDTO.getValor());

            return saidaPagamentoDTO;
        }

        throw new RuntimeException("Pagamento invalido");
    }


    public List<SaidaPagamentoDTO> consultaPagtosPorId(Integer id_cartao){

        Cartao objCartao = cartaoService.consultarCartaoPorId(id_cartao);

        if (objCartao == null){

            throw new RuntimeException("Cartão não encontrado");
        }else{

            Iterable<Pagamento> objPagamento = pagamentoRepository.findAllByCartao(objCartao);
            List<SaidaPagamentoDTO> objSaida = new ArrayList<>();
            SaidaPagamentoDTO saidaPagamentoDTO;

            for (Pagamento pagto:objPagamento) {

                saidaPagamentoDTO = new SaidaPagamentoDTO(pagto.getId(), pagto.getCartao().getId(),
                        pagto.getDescrição(), pagto.getValor());
                objSaida.add(saidaPagamentoDTO);
            }
            return objSaida;
        }
    }

}
