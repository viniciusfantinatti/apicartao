package br.com.itau.PropostaCartao.services;

import br.com.itau.PropostaCartao.models.Cliente;
import br.com.itau.PropostaCartao.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente incluirCliente(Cliente cliente){

        Cliente objCliente = clienteRepository.save(cliente);

        return objCliente;
    }

    public Cliente buscarClientePorId(Integer id){

        Optional<Cliente> optCliente = clienteRepository.findById(id);

        if(optCliente.isPresent()){

            return optCliente.get();
        }else {

            throw new RuntimeException("Cliente não encontrado!");
        }
    }
}
