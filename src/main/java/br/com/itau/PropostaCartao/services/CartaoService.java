package br.com.itau.PropostaCartao.services;

import br.com.itau.PropostaCartao.DTOs.AtualizaCartaoDTO;
import br.com.itau.PropostaCartao.DTOs.CadastraCartaoDTO;
import br.com.itau.PropostaCartao.DTOs.ConsultaCartaoDTO;
import br.com.itau.PropostaCartao.DTOs.SaidaCartaoDTO;
import br.com.itau.PropostaCartao.models.Cartao;
import br.com.itau.PropostaCartao.models.Cliente;
import br.com.itau.PropostaCartao.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteService clienteService;



    public SaidaCartaoDTO incluirCartao(CadastraCartaoDTO cadastraCartaoDTO){

        Cliente objCliente = clienteService.buscarClientePorId(cadastraCartaoDTO.getClienteId());

        Cartao objCartao = new Cartao();
        objCartao.setCliente(objCliente);
        objCartao.setNumero(cadastraCartaoDTO.getNumero());
        objCartao.setAtivo(Boolean.FALSE);
        cartaoRepository.save(objCartao);

        SaidaCartaoDTO saidaCartaoDTO = new SaidaCartaoDTO();
        saidaCartaoDTO.setId(objCartao.getId());
        saidaCartaoDTO.setNumero(objCartao.getNumero());
        saidaCartaoDTO.setClienteId(objCliente.getId());
        saidaCartaoDTO.setAtivo(objCartao.getAtivo());

        return saidaCartaoDTO;
    }

    public ConsultaCartaoDTO consultarCartaoPorNumero(String numero){

        Optional<Cartao> optCartao = cartaoRepository.findByNumero(numero);

        if (optCartao.isPresent()){

            ConsultaCartaoDTO consultaCartaoDTO = new ConsultaCartaoDTO();
            consultaCartaoDTO.setId(optCartao.get().getId());
            consultaCartaoDTO.setNumero(optCartao.get().getNumero());
            consultaCartaoDTO.setClienteId(optCartao.get().getCliente().getId());

            return consultaCartaoDTO;
        }else{

            throw new RuntimeException("Cartão não encontrado");
        }
    }

    public SaidaCartaoDTO atualizaCartao(String numero, AtualizaCartaoDTO atualizaCartaoDTO){

        Optional<Cartao> optCartao = cartaoRepository.findByNumero(numero);

        if (optCartao.isPresent()){

            Cartao objCartao = new Cartao();
            objCartao.setId(optCartao.get().getId());
            objCartao.setNumero(optCartao.get().getNumero());
            objCartao.setCliente(optCartao.get().getCliente());
            objCartao.setAtivo(atualizaCartaoDTO.getAtivo());
            cartaoRepository.save(objCartao);

            SaidaCartaoDTO saidaCartaoDTO = new SaidaCartaoDTO();
            saidaCartaoDTO.setId(objCartao.getId());
            saidaCartaoDTO.setNumero(objCartao.getNumero());
            saidaCartaoDTO.setClienteId(objCartao.getCliente().getId());
            saidaCartaoDTO.setAtivo(objCartao.getAtivo());

            return saidaCartaoDTO;
        }else{

            throw new RuntimeException("Cartão não encontrado");
        }
    }

    public Cartao consultarCartaoPorId (Integer id){

        Optional<Cartao> optCartao = cartaoRepository.findById(id);

        if (optCartao.isPresent()){

            return optCartao.get();
        }else{

            throw new RuntimeException("Cartão não encontrado");
        }

    }
}
