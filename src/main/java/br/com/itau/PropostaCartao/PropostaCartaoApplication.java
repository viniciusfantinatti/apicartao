package br.com.itau.PropostaCartao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PropostaCartaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PropostaCartaoApplication.class, args);
	}

}
