package br.com.itau.PropostaCartao.models;
import javax.persistence.*;

@Entity
public class Cartao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(length = 5)
    private Integer id;

    @Column(length = 9)
    private String numero;

    @ManyToOne(cascade = CascadeType.ALL)
    private  Cliente cliente;

    private Boolean ativo;

    public Cartao() {
    }

    public Cartao(Integer id, String numero, Cliente cliente, Boolean ativo) {
        this.id = id;
        this.numero = numero;
        this.cliente = cliente;
        this.ativo = ativo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}
