package br.com.itau.PropostaCartao.models;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
public class Pagamento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(length = 5)
    private Integer id;

    @ManyToOne(cascade = CascadeType.ALL)
    private Cartao cartao;

    @Column(length = 20)
    @NotNull(message = "Descrição não pode ser nula")
    @NotEmpty(message = "Descrição não pode ser vazia")
    private String descrição;

    @Column(length = 12, precision = 10, scale = 2)
    private Double valor;

    public Pagamento() {
    }

    public Pagamento(Integer id, Cartao cartao, String descrição, Double valor) {
        this.id = id;
        this.cartao = cartao;
        this.descrição = descrição;
        this.valor = valor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Cartao getCartao() {
        return cartao;
    }

    public void setCartao(Cartao cartao) {
        this.cartao = cartao;
    }

    public String getDescrição() {
        return descrição;
    }

    public void setDescrição(String descrição) {
        this.descrição = descrição;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
