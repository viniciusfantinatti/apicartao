package br.com.itau.PropostaCartao.controllers;

import br.com.itau.PropostaCartao.DTOs.EntradaPagamentoDTO;
import br.com.itau.PropostaCartao.DTOs.SaidaPagamentoDTO;
import br.com.itau.PropostaCartao.models.Pagamento;
import br.com.itau.PropostaCartao.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;


    //INCLUSAO
    @PostMapping("/pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public SaidaPagamentoDTO incluiPagamento(@RequestBody EntradaPagamentoDTO entradaPagamentoDTO){

        try {

            SaidaPagamentoDTO saidaPagamentoDTO = pagamentoService.incluirPagamento(entradaPagamentoDTO);
            return saidaPagamentoDTO;
        }catch (RuntimeException e){

            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    //CONSULTA
    @GetMapping("/pagamentos/{id_cartao}")
    @ResponseStatus(HttpStatus.OK)
    public List<SaidaPagamentoDTO> consultaPagamentosPorCartao(@PathVariable(name = "id_cartao") Integer id_cartao){

        try{

            List<SaidaPagamentoDTO> objPagto = pagamentoService.consultaPagtosPorId(id_cartao);
            return objPagto;
        }catch (RuntimeException e){

            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
