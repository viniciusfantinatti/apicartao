package br.com.itau.PropostaCartao.controllers;

import br.com.itau.PropostaCartao.models.Cliente;
import br.com.itau.PropostaCartao.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;


    //INCLUSÃO
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente incluirCliente(@RequestBody @Valid Cliente cliente){

        Cliente objCliente = clienteService.incluirCliente(cliente);

        return objCliente;
    }

    //CONSULTA
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Cliente consultarClientePorId(@PathVariable(name = "id") Integer id){

        try{

            Cliente objCliente = clienteService.buscarClientePorId(id);

            return objCliente;
        }catch(RuntimeException e){

            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
