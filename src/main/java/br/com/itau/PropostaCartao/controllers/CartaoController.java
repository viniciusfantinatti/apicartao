package br.com.itau.PropostaCartao.controllers;

import br.com.itau.PropostaCartao.DTOs.AtualizaCartaoDTO;
import br.com.itau.PropostaCartao.DTOs.CadastraCartaoDTO;
import br.com.itau.PropostaCartao.DTOs.ConsultaCartaoDTO;
import br.com.itau.PropostaCartao.DTOs.SaidaCartaoDTO;
import br.com.itau.PropostaCartao.models.Cartao;
import br.com.itau.PropostaCartao.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;


    //INCLUSAO

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public SaidaCartaoDTO incluirCartao(@RequestBody @Valid CadastraCartaoDTO cadastraCartaoDTO){

        try{

            SaidaCartaoDTO objCartao = cartaoService.incluirCartao(cadastraCartaoDTO);

            return objCartao;
        }catch (RuntimeException erro){

            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, erro.getMessage());
        }
    }

    //CONSULTA
    @GetMapping("/{numero}")
    @ResponseStatus(HttpStatus.OK)
    public ConsultaCartaoDTO consultarCartaoPorNumero(@PathVariable(name = "numero") String numero){

        try{

            ConsultaCartaoDTO objCartao = cartaoService.consultarCartaoPorNumero(numero);
            return objCartao;
        }catch (RuntimeException e){

            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    //ATUALIZA
    @PatchMapping("/{numero}")
    @ResponseStatus(HttpStatus.OK)
    public SaidaCartaoDTO atualizaCartao(@PathVariable(name = "numero") String numero,
                                 @RequestBody AtualizaCartaoDTO atualizaCartaoDTO){

        try{

            SaidaCartaoDTO objCartao = cartaoService.atualizaCartao(numero, atualizaCartaoDTO);
            return objCartao;
        }catch (RuntimeException e){

            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }
}
