package br.com.itau.PropostaCartao.repositories;

import br.com.itau.PropostaCartao.models.Cartao;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {

    Optional<Cartao> findByNumero(String numero);
}
