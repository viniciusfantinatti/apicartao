package br.com.itau.PropostaCartao.repositories;

import br.com.itau.PropostaCartao.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
}
