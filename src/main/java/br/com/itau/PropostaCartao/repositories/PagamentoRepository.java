package br.com.itau.PropostaCartao.repositories;

import br.com.itau.PropostaCartao.models.Cartao;
import br.com.itau.PropostaCartao.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {

    Iterable<Pagamento> findAllByCartao(Cartao cartao);
}
