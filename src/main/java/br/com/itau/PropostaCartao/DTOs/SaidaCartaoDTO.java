package br.com.itau.PropostaCartao.DTOs;

public class SaidaCartaoDTO {

    private Integer id;

    private String numero;

    private Integer clienteId;

    private Boolean ativo;

    public SaidaCartaoDTO() {
    }

    public SaidaCartaoDTO(Integer id, String numero, Integer clienteId, Boolean ativo) {
        this.id = id;
        this.numero = numero;
        this.clienteId = clienteId;
        this.ativo = ativo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}
