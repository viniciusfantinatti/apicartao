package br.com.itau.PropostaCartao.DTOs;

public class AtualizaCartaoDTO {

    private Boolean ativo;

    public AtualizaCartaoDTO() {
    }

    public AtualizaCartaoDTO(Boolean ativo) {
        this.ativo = ativo;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}
