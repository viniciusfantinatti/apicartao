package br.com.itau.PropostaCartao.DTOs;

import br.com.itau.PropostaCartao.models.Cartao;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class SaidaPagamentoDTO {

    private Integer id;

    private Integer cartao_id;

    private String descricao;

    private Double valor;

    public SaidaPagamentoDTO() {
    }

    public SaidaPagamentoDTO(Integer id, Integer cartao_id, String descricao, Double valor) {
        this.id = id;
        this.cartao_id = cartao_id;
        this.descricao = descricao;
        this.valor = valor;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCartao_id() {
        return cartao_id;
    }

    public void setCartao_id(Integer cartao_id) {
        this.cartao_id = cartao_id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

//    public void setValor(Double valor) {
//        BigDecimal bigDecimalValor = new BigDecimal(valor).setScale(2,RoundingMode.FLOOR);
//        this.valor = bigDecimalValor.doubleValue();
//    }


    public void setValor(Double valor) {
        this.valor = valor;
    }
}
