package br.com.itau.PropostaCartao.DTOs;


public class CadastraCartaoDTO {

    private String numero;

    private Integer clienteId;


    public CadastraCartaoDTO() {
    }

    public CadastraCartaoDTO(String numero, Integer clienteId) {
        this.numero = numero;
        this.clienteId = clienteId;

    }


    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }
}
